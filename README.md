# JSONC Converter

JSONC (JSON with Comments) to JSON and back converter which keeps comments.

Utility for converting between [JSONC](https://code.visualstudio.com/docs/languages/json#_json-with-comments) and [JSON](https://www.json.org/json-en.html) formats without loosing the comments.

## TODO

-   [ ] cli
-   [x] export interface
-   [x] single line comment before key
-   [x] block comment before key
-   [x] single line comment after key
-   [x] block comment after key
-   [ ] single line comment before array items
-   [ ] options
    -   [x] custom comments suffix
    -   [ ] custom JSON.parse whitespace
    -   [ ] optional new line at the end

## Usage

```bash
npm install -g jsonc-converter
jsonc-converter <JSON_FILE> > <JSONC_OUTPUT>
jsonc-converter <JSONC_FILE> -c > <JSON_OUTPUT>
```
