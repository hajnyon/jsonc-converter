module.exports = {
    'extends': '@istanbuljs/nyc-config-typescript',
    'check-coverage': true,
    'all': true,
    'include': ['src/**/*.ts'],
    'exclude': ['test/**/*.spec.ts', '**/*.d.ts', 'src/**/index.ts'],
    'require': ['ts-node/register', 'source-map-support/register'],
    'reporter': ['text', 'html']
};
