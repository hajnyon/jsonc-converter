import * as fs from 'fs';
import * as path from 'path';

export function getFile(name: string, extension: string = 'jsonc'): string {
    return fs.readFileSync(path.join(__dirname, 'fixtures', extension, `${name}.${extension}`), { encoding: 'utf-8' });
}
