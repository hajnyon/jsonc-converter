import test, { ExecutionContext } from 'ava';
import { Edit } from 'jsonc-parser';

import { JsoncEdit } from '../../../../src/converter/jsonc-to-json/jsonc-edit/jsonc-edit.model';
import { ECommentPosition } from '../../../../src/interfaces/comment-position.enum';
import { ECommentType } from '../../../../src/interfaces/comment-type.enum';

test('isComplete: correctly controls completion', (t: ExecutionContext) => {
    const jsoncEdit: JsoncEdit = new JsoncEdit();

    t.false(jsoncEdit.isComplete());

    jsoncEdit.data = {
        offset: 1,
        length: 2,
        comment: 'comment',
        type: ECommentType.LINE,
        position: ECommentPosition.BEFORE_PROPERTY
    };
    t.false(jsoncEdit.isComplete());

    jsoncEdit.data.key = 'key';
    jsoncEdit.data.keyOffset = 1;
    t.true(jsoncEdit.isComplete());
});

test('getEdit: gets full edit', (t: ExecutionContext) => {
    const jsoncEdit: JsoncEdit = new JsoncEdit();

    jsoncEdit.data = {
        offset: 1,
        length: 2,
        comment: 'comment',
        type: ECommentType.LINE,
        position: ECommentPosition.BEFORE_PROPERTY,
        key: 'key',
        keyOffset: 1
    };

    const commentSuffix: string = '__comment';
    const expected: Edit = {
        offset: 1,
        length: 2,
        content: `"key${commentSuffix}":{"before":"comment"},`
    };

    t.deepEqual(jsoncEdit.getEdit(commentSuffix), [expected]);
});

test('getEdit: throws when not complete', (t: ExecutionContext) => {
    const jsoncEdit: JsoncEdit = new JsoncEdit();

    jsoncEdit.data = {
        offset: 1,
        length: 2,
        comment: 'comment',
        type: ECommentType.LINE,
        position: ECommentPosition.BEFORE_PROPERTY
    };

    const error: Error = t.throws<Error>(() => jsoncEdit.getEdit(''));
    t.true(error instanceof Error);
});
