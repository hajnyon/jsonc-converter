import test, { ExecutionContext } from 'ava';

import { JsoncEdits } from '../../../../src/converter/jsonc-to-json/jsonc-edit/jsonc-edits.model';
import { ECommentPosition } from '../../../../src/interfaces/comment-position.enum';
import { ECommentType } from '../../../../src/interfaces/comment-type.enum';

test('adds single edit', (t: ExecutionContext) => {
    const edits: JsoncEdits = new JsoncEdits('');
    t.deepEqual(edits.getEdits(), []);

    edits.addEditProperties({ key: 'key', keyOffset: 0 });
    t.deepEqual(edits.getEdits(), []);

    edits.addEditProperties({ offset: 0, length: 0, comment: '', position: ECommentPosition.BEFORE_PROPERTY, type: ECommentType.LINE });
    t.deepEqual(edits.getEdits(), [{ offset: 0, length: 0, content: `"key":{"before":""},` }]);
});

test('adds multiple edits', (t: ExecutionContext) => {
    const edits: JsoncEdits = new JsoncEdits('');
    t.deepEqual(edits.getEdits(), []);

    edits.addEditProperties({
        key: 'key',
        keyOffset: 0,
        offset: 0,
        length: 0,
        comment: '',
        position: ECommentPosition.BEFORE_PROPERTY,
        type: ECommentType.LINE
    });
    t.deepEqual(edits.getEdits(), [{ offset: 0, length: 0, content: `"key":{"before":""},` }]);

    edits.addEditProperties({
        key: 'key',
        keyOffset: 0,
        offset: 0,
        length: 0,
        comment: '',
        position: ECommentPosition.BEFORE_PROPERTY,
        type: ECommentType.LINE
    });
    t.deepEqual(edits.getEdits(), [
        { offset: 0, length: 0, content: `"key":{"before":""},` },
        { offset: 0, length: 0, content: `"key":{"before":""},` }
    ]);
});

test('adds multiple edits with end of line comment', (t: ExecutionContext) => {
    const edits: JsoncEdits = new JsoncEdits('');
    t.deepEqual(edits.getEdits(), []);

    edits.addEditProperties({
        key: 'key',
        keyOffset: 0,
        offset: 0,
        length: 0,
        comment: '',
        position: ECommentPosition.BEFORE_PROPERTY,
        type: ECommentType.LINE
    });
    t.deepEqual(edits.getEdits(), [{ offset: 0, length: 0, content: `"key":{"before":""},` }]);

    edits.addEditProperties({ offset: 0, length: 0, comment: '', position: ECommentPosition.END_OF_LINE, type: ECommentType.LINE });
    t.deepEqual(edits.getEdits(), [
        { offset: 0, length: 0, content: `"key":{"before":"","eol":""},` },
        { offset: 0, length: 0, content: '' }
    ]);
});
