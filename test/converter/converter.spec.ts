import test, { ExecutionContext } from 'ava';

import { jsoncToJson, jsonToJsonc } from '../../src/converter';
import { getFile } from '../helpers';

const fixtures: string[] = ['simple', 'basic', 'block'];

for (const fixture of fixtures) {
    test(`jsoncToJson: ${fixture}`, (t: ExecutionContext) => {
        const jsonc: string = getFile(fixture, 'jsonc');
        const json: string = getFile(fixture, 'json');
        t.deepEqual(JSON.parse(jsoncToJson(jsonc)), JSON.parse(json));
    });
    test(`jsonToJsonc: ${fixture}`, (t: ExecutionContext) => {
        const jsonc: string = getFile(fixture, 'jsonc');
        const json: string = JSON.parse(getFile(fixture, 'json'));
        t.is(jsonToJsonc(json), jsonc);
    });
}

test('jsoncToJson: invalid jsonc throws', (t: ExecutionContext) => {
    const jsonc: string = `{
    "key": "value", // comment
}`;
    const error: Error = t.throws<Error>(() => jsoncToJson(jsonc));
    t.true(error instanceof Error);
});

test('jsonToJsonc: invalid input data throws', (t: ExecutionContext) => {
    // circular reference (JSON.stringify should throw)
    const data: any = {};
    data.myself = data;

    const error: Error = t.throws<Error>(() => jsonToJsonc(data));
    t.true(error instanceof Error);
});
