import test, { ExecutionContext } from 'ava';
import { stdout } from 'test-console';

import { cli } from '../src/cli';
import { getFile } from './helpers';

test(`cli: json to jsonc simple`, (t: ExecutionContext) => {
    const jsonc: string = getFile('simple', 'jsonc');
    const output = stdout.inspectSync(() => {
        cli(['node', 'jsonc-converter', 'test/fixtures/json/simple.json']);
    });
    t.deepEqual(output[0], jsonc);
});

test(`cli: jsonc to json simple`, (t: ExecutionContext) => {
    const json: string = getFile('simple', 'json');
    const output = stdout.inspectSync(() => {
        cli(['node', 'jsonc-converter', 'test/fixtures/jsonc/simple.jsonc', '-c']);
    });
    t.deepEqual(JSON.parse(output[0]), JSON.parse(json));
});

test(`cli: throws without path`, (t: ExecutionContext) => {
    const error: Error = t.throws<Error>(() => cli([]));
    t.true(error instanceof Error);
});

test(`cli: throws with non-parsable json file`, (t: ExecutionContext) => {
    const error: Error = t.throws<Error>(() => cli(['node', 'jsonc-converter', 'test/fixtures/not-json.json']));
    t.true(error instanceof Error);
});
