import * as fs from 'fs';
import * as path from 'path';

import arg from 'arg';

import { jsoncToJson, jsonToJsonc } from './converter/jsonc.converter';
import { IOptions } from './interfaces/options.interface';
import { DEFAULT_OPTIONS } from './model/options.model';

function parseArgumentsIntoOptions(rawArgs: string[]) {
    const args = arg(
        {
            '--jsonc': Boolean,
            '--suffix': String,
            '--indentation': String,
            '-c': '--jsonc',
            '-s': '--suffix',
            '-i': '--indentation'
        },
        {
            argv: rawArgs.slice(2)
        }
    );
    return {
        jsoncInput: args['--jsonc'] || false,
        commentSuffix: args['--suffix'] || DEFAULT_OPTIONS.commentSuffix,
        indentation: args['--indentation'] || DEFAULT_OPTIONS.indentation,
        path: args._[0]
    };
}

export function cli(args: string[]): void {
    let options = parseArgumentsIntoOptions(args);
    if (options.path === undefined) {
        throw new Error('No path to file specified.');
    }

    const inputPath: string = path.join(process.cwd(), options.path);
    const fileContent: string = fs.readFileSync(inputPath, { encoding: 'utf-8' });
    const converterOptions: IOptions = {
        commentSuffix: options.commentSuffix,
        indentation: options.indentation
    };

    let result: string;
    if (options.jsoncInput) {
        result = jsoncToJson(fileContent, converterOptions);
    } else {
        let data: any;
        try {
            data = JSON.parse(fileContent);
        } catch (error) {
            throw new Error(`Can't parse input file.`);
        }
        result = jsonToJsonc(data);
    }
    process.stdout.write(result);
}
