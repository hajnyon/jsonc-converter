export interface IComment {
    before?: string;
    eol?: string;
}
