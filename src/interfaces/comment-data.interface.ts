import { ECommentPosition } from './comment-position.enum';
import { ECommentType } from './comment-type.enum';

export interface ICommentData {
    content: string;
    type: ECommentType;
    position: ECommentPosition;
}
