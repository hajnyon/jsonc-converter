export interface IOptions {
    commentSuffix: string;
    indentation: string;
}
