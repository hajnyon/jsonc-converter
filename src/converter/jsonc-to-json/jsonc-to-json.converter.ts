import { applyEdits, Edit, ParseErrorCode, visit } from 'jsonc-parser';

import { ECommentPosition } from '../../interfaces/comment-position.enum';
import { ECommentType } from '../../interfaces/comment-type.enum';
import { IOptions } from '../../interfaces/options.interface';
import { JsoncEdits } from './jsonc-edit/jsonc-edits.model';

export class JsoncToJsonConverter {
    private static readonly WHITESPACE_REGEX = /\S/;

    public static convert(jsonc: string, options: IOptions): string {
        const edits: JsoncEdits = new JsoncEdits(options.commentSuffix);

        visit(jsonc, {
            onObjectProperty: (property: string, offset: number) => {
                edits.addEditProperties({ key: property, keyOffset: offset });
            },
            onComment: (offset: number, length: number, _startLine: number, startCharacter: number) => {
                const content: string = jsonc.slice(offset, offset + length);
                const type: ECommentType = content.startsWith('//') ? ECommentType.LINE : ECommentType.BLOCK;
                const beforeComment: string = jsonc.slice(offset - startCharacter, offset);
                const position: ECommentPosition = JsoncToJsonConverter.WHITESPACE_REGEX.test(beforeComment)
                    ? ECommentPosition.END_OF_LINE
                    : ECommentPosition.BEFORE_PROPERTY;
                edits.addEditProperties({ length: length, offset: offset, comment: content, type: type, position: position });
            },
            onObjectBegin: () => {
                edits.addEditProperties({ key: undefined, keyOffset: undefined });
            },
            onError: (error: ParseErrorCode, offset: number, _length: number, startLine: number, startCharacter: number) => {
                throw new Error(
                    `Input jsonc is malformed. Error code: ${error}, offset: ${offset}, startLine: ${startLine}, startCharacter: ${startCharacter}`
                );
            }
        });
        const editItems: Edit[] = edits.getEdits();
        const editedString: string = applyEdits(jsonc, editItems);
        return editedString;
    }
}
