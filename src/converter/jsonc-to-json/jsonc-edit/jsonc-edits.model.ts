import { Edit } from 'jsonc-parser';

import { ECommentPosition } from '../../../interfaces/comment-position.enum';
import { IJsoncEditData } from './jsonc-edit.interface';
import { JsoncEdit } from './jsonc-edit.model';

export class JsoncEdits {
    private edits: Edit[] = [];
    private currentEdit: JsoncEdit = new JsoncEdit();
    private previousEdit: JsoncEdit | null = null;

    public constructor(private readonly commentKey: string) {}

    public addEditProperties(properties: IJsoncEditData): void {
        if (
            properties.position &&
            properties.position === ECommentPosition.END_OF_LINE &&
            this.currentEdit.data.key === undefined &&
            this.previousEdit
        ) {
            // TODO: refactor this
            const edit: JsoncEdit = new JsoncEdit();
            edit.data = { ...this.previousEdit.data, ...properties };
            const previousEdit: Edit | undefined = this.edits.pop();
            if (!previousEdit) {
                throw new Error();
            }
            const previousContent: string = previousEdit.content.slice(0, -2);
            previousEdit.content = `${previousContent},"eol":"${edit.data.comment}"},`;
            this.edits.push(previousEdit);
            this.edits.push({
                offset: <number>edit.data.offset,
                length: <number>edit.data.length,
                content: ''
            });
            return;
        }

        this.currentEdit.data = { ...this.currentEdit.data, ...properties };
        if (this.currentEdit.isComplete()) {
            this.edits.push(...this.currentEdit.getEdit(this.commentKey));
            this.previousEdit = this.currentEdit;
            this.currentEdit = new JsoncEdit();
        }
    }
    public getEdits(): Edit[] {
        return this.edits;
    }
}
