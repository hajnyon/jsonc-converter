import { Edit } from 'jsonc-parser';

import { ECommentPosition } from '../../../interfaces/comment-position.enum';
import { IJsoncEditData } from './jsonc-edit.interface';

export class JsoncEdit {
    public data: IJsoncEditData = {};

    public isComplete(): boolean {
        if (
            this.data.length === undefined ||
            this.data.offset === undefined ||
            this.data.comment === undefined ||
            this.data.type === undefined ||
            this.data.position === undefined ||
            this.data.key === undefined ||
            this.data.keyOffset === undefined
        ) {
            return false;
        }
        return true;
    }

    public getEdit(commentKey: string): Edit[] {
        if (!this.isComplete()) {
            throw new Error('Edit not complete.');
        }

        // before property comment
        if (this.data.position === ECommentPosition.BEFORE_PROPERTY) {
            return [
                {
                    length: <number>this.data.length,
                    offset: <number>this.data.offset,
                    content: `"${this.data.key}${commentKey}":{"before":"${this.data.comment}"},`
                }
            ];
        }

        // end of line comment
        return [
            {
                length: 0,
                offset: <number>this.data.keyOffset,
                content: `"${this.data.key}${commentKey}":{"eol":"${this.data.comment}"},`
            },
            {
                length: <number>this.data.length,
                offset: <number>this.data.offset,
                content: ''
            }
        ];
    }
}
