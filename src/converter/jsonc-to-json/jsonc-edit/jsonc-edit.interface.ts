import { ECommentPosition } from '../../../interfaces/comment-position.enum';
import { ECommentType } from '../../../interfaces/comment-type.enum';

export interface IJsoncEditData {
    length?: number;
    offset?: number;
    comment?: string;
    type?: ECommentType;
    position?: ECommentPosition;
    key?: string;
    keyOffset?: number;
}
