import Queue from '@iter-tools/queue';
import { applyEdits, Edit, visit } from 'jsonc-parser';

import { IComment } from '../../interfaces/comment.interface';

export class JsonToJsoncConverter {
    private static stringify(jsonInput: string): { formattedJson: string; comments: Queue<IComment | undefined> } {
        const comments: Queue<IComment | undefined> = new Queue();
        let lastKeyWasComment: boolean = false;
        let formattedJson: string = '';
        try {
            formattedJson = JSON.stringify(
                jsonInput,
                (key: string, value: any) => {
                    if (/.*__comment/.test(key)) {
                        comments.push(value);
                        lastKeyWasComment = true;
                        return undefined;
                    }
                    if (lastKeyWasComment) {
                        lastKeyWasComment = false;
                    } else {
                        comments.push(undefined);
                    }
                    return value;
                },
                4
            );
        } catch (error) {
            throw new Error(`Input data cannot be stringified: ${error.message}`);
        }
        return { formattedJson: formattedJson, comments: comments };
    }

    public static convert(jsonInput: string): any {
        const { formattedJson, comments } = JsonToJsoncConverter.stringify(jsonInput);
        const edits: Edit[] = [];

        // remove first (replacer matches first root as key)
        comments.shift();
        let eolComment: string | undefined = undefined;

        visit(formattedJson, {
            onObjectProperty: (_property: string, offset: number, _length: number, _startLine: number, startCharacter: number) => {
                const whiteSpace: string = formattedJson.slice(offset - startCharacter, offset);
                const comment: IComment | undefined = comments.shift();
                if (comment === undefined) {
                    return;
                }
                if (comment.before) {
                    edits.push({
                        offset: offset,
                        length: 0,
                        content: `${comment.before}\n${whiteSpace}`
                    });
                }
                eolComment = comment.eol;
            },
            onLiteralValue: (_value: any, offset: number, length: number, _startLine: number, _startCharacter: number) => {
                if (eolComment) {
                    edits.push({
                        offset: offset + length + 1,
                        length: 0,
                        content: ` ${eolComment}`
                    });
                    eolComment = undefined;
                }
            }
        });
        return applyEdits(formattedJson, edits);
    }
}
