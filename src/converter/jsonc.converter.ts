import { IOptions } from '../interfaces/options.interface';
import { DEFAULT_OPTIONS } from '../model/options.model';
import { JsonToJsoncConverter } from './json-to-jsonc/json-to-jsonc.converter';
import { JsoncToJsonConverter } from './jsonc-to-json/jsonc-to-json.converter';

export function jsoncToJson(jsonc: string, userOptions: Partial<IOptions> = {}): string {
    const options = { ...DEFAULT_OPTIONS, ...userOptions };
    return JsoncToJsonConverter.convert(jsonc, options);
}

export function jsonToJsonc(jsonInput: any): string {
    return JsonToJsoncConverter.convert(jsonInput);
}
