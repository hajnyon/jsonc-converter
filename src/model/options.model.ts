import { IOptions } from '../interfaces/options.interface';

export const DEFAULT_OPTIONS: IOptions = {
    commentSuffix: '__comment',
    indentation: '\t'
};
