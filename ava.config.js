export default {
    require: ['ts-node/register'],
    extensions: ['.ts'],
    files: ['test/**/*.spec.ts'],
    typescript: {
        rewritePaths: {
            'src/': 'build/'
        }
    }
};
